variable "prefix" {
  default = "app"
}

variable "project" {
  default = "django-app-devops"
}

variable "contact" {
  default = "orkoshnelisaba@gmail.com"
}

variable "db_host" {
  default = "database-2.ce5douh6ctde.eu-central-1.rds.amazonaws.com"
}

variable "db_username" {
  default = "postgres"
}

variable "db_password" {
  default = "sabaorkpostgres"
}

variable "db_name" {
  default = "database-2"
}

variable "bastion_key_name" {
  default = "django-app-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "583118230908.dkr.ecr.eu-central-1.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for proxy"
  default     = "583118230908.dkr.ecr.eu-central-1.amazonaws.com/django-app-nginx-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django APP"
}

variable "dns_zone_name" {
  description = "Domain name"
  default     = "sabaork.click"
}

variable "subdomain" {
  description = "Subdomain per enviroment"
  type        = map(string)
  default = {
    production = "api"
    staging    = "api.staging"
    dev        = "api.dev"
  }
}