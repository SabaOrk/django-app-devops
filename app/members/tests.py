from django.test import TestCase, Client
from django.urls import reverse
from members.models import Bank, FinancialData
import json



class TestMembersViews(TestCase):

    def setUp(self):
        self.client = Client()
        self.get_all_member_banks_url = reverse('get_all_member_banks')
        self.get_member_bank_url = reverse('get_member_bank')

        bank = Bank.objects.create(name='test_bank',
                                   description='test bank description',
                                   contact_info='test bank contanct info')

    def test_get_all_member_banks_views(self):

        response = self.client.get(self.get_all_member_banks_url)
        self.assertEquals(response.status_code, 200)
    
    def test_get_member_bank_views(self):

        response = self.client.post(self.get_member_bank_url, {'name': 'test_bank'})
        self.assertEquals(response.status_code, 200)


