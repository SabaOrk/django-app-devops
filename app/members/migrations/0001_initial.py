# Generated by Django 3.2.13 on 2022-05-11 19:09

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='FinancialData',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('document', models.FileField(upload_to='members/banks/documents/')),
                ('document_year', models.IntegerField(default=2022)),
            ],
        ),
        migrations.CreateModel(
            name='Bank',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('description', models.TextField(blank=True, max_length=1000, null=True)),
                ('photo', models.ImageField(blank=True, null=True, upload_to='members/banks/')),
                ('contact_info', models.CharField(max_length=255)),
                ('financial_data', models.ManyToManyField(to='members.FinancialData')),
            ],
        ),
    ]
