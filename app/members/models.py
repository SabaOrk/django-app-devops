from django.db import models


# Create your models here.
class FinancialData(models.Model):
    document = models.FileField(upload_to='members/banks/documents/')
    document_year = models.IntegerField(default=2022)

    def __str__(self) -> str:
        return f"{self.document_year}"

class Bank(models.Model):
    name = models.CharField(max_length=255, blank=False, null=False)
    description = models.TextField(max_length=1000, default='', blank=True, null=True)
    photo = models.ImageField(null=True, blank=True, upload_to='members/banks/')
    contact_info = models.CharField(max_length=255, blank=False, null=False)
    corporate_gov = models.CharField(max_length=255, default='', blank=False, null=False)
    financial_data = models.ManyToManyField(FinancialData, related_name="financial_data")
    
    def __str__(self) -> str:
        return f"{self.name}" 

