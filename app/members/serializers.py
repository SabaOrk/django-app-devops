from django.conf import settings
from .models import Bank

def serialize_member_bank(bank):

    res = {
        'id': bank.id,
        'name': bank.name,
        'description': bank.description,
        'contact_info':bank.contact_info,
        'photo': bank.photo.url if bank.photo else ''
           }
    return res

def serialize_financial_data(data):
    res = {
        'id': data.id,
        'document': data.document.url,
        'document_year': data.document_year,
           }
    return res