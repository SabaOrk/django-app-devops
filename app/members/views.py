from rest_framework.decorators import api_view
from rest_framework.response import Response

from . import serializers
from django.conf import settings
from .models import Bank, FinancialData

@api_view(['GET'])
def get_all_member_banks(request):

    banks = Bank.objects.all()
    data = [serializers.serialize_member_bank(bank) for bank in banks]

    result = {
        'data': data
    }
    
    return Response(result)

@api_view(['POST'])
def get_member_bank(request):

    name = request.data['name']
    bank = Bank.objects.get(name=name)
    data = serializers.serialize_member_bank(bank)

    result = {
        'data': data
    }

    return Response(result)


@api_view(['GET'])
def get_financial_data_ordered_by_bank(request):

    all_banks = Bank.objects.all()
    data = []

    for bank in all_banks:
        financial_data = FinancialData.objects.all().filter(financial_data=bank)
        data.append({'bank': bank.name, 
                     'photo': bank.photo.url,
                     'financial_data' : [serializers.serialize_financial_data(data) for data in financial_data]})

    result = {
        'data': data
    }

    return Response(result)
