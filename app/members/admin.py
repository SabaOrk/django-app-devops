from django.contrib import admin
from .models import Bank, FinancialData

# Register your models here.
#admin.site.register(Bank)
#admin.site.register(FinancialData)


@admin.register(Bank)
class BankAdmin(admin.ModelAdmin):
    pass


@admin.register(FinancialData)
class FinancialDataAdmin(admin.ModelAdmin):
    pass