from django.urls import path
from . import views

urlpatterns = [
    path('bank/', views.get_member_bank, name='get_member_bank'),
    path('banks/', views.get_all_member_banks, name='get_all_member_banks'),
    path('financial_data/', views.get_financial_data_ordered_by_bank, name='get_financial_data_ordered_by_bank'),
]