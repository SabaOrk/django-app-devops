

def serialize_article(article):
    
    return {
        'id':article.id,
        'header':article.header,
        'url_title':article.url_title,
        'title': article.title,
        'date':article.date_created.strftime('%b. %d, %Y'),
        'category': article.category.name if article.category else '',
        'photo': article.picture.url if article.picture else ''
    }


def serialize_full_article(article):
    
    return {
        'id':article.id,
        'header':article.header,
        'title': article.title,
        'date':article.date_created.strftime('%b. %d, %Y'),
        'category': article.category.name if article.category else '',
        'photo': article.picture.url if article.picture else '',
        'body':article.body,
        'url_title':article.url_title,
        'meta_title':article.meta_title,
        'meta_description':article.meta_description
    }

def serialize_standard(obj):
    return {
        "title":obj.title,
        "header":obj.header,
        "date":obj.date_created.strftime('%b. %d, %Y'),
        "pdf":obj.pdf.url if obj.pdf else "",
        "word":obj.word.url if obj.word else "",
        "web":obj.web.url if obj.web else ""
    }

def serialize_eco(obj):
    return {
        "header":obj.header,
        "body":obj.body,
        "meta_description":obj.meta_description,
    }

def serialize_join(obj):
    return {
        "id": obj.id,
        "title":obj.title,
        "body":obj.body,
    }