from django.db import models
from datetime import date
from django.db.models.signals import post_save
from django.dispatch import receiver
from tinymce import models as tinymce_models
from .unzip_s3 import unzip_file

# Create your models here.

class News(models.Model):
    date_created = models.DateField(default=date.today)
    title = models.CharField(max_length=120)
    header = models.TextField()
    body = tinymce_models.HTMLField()
    picture = models.ImageField(null=True, blank=True, upload_to='articles/pictures')
    url_title = models.CharField(max_length=120)
    meta_title = models.CharField(max_length=255)
    meta_description = models.CharField(max_length=255)
    

    def __str__(self) -> str:
        return f"{self.title}" 


class Standard(models.Model):
    title = models.CharField(max_length=120)
    header = models.TextField()
    pdf = models.FileField(upload_to='openbanking/standards/pdf/')
    word = models.FileField(upload_to='openbanking/standards/word/')
    web = models.FileField(upload_to='openbanking/standards/web/')
    date_created = models.DateField(default=date.today)

@receiver(post_save, sender=Standard)
def post_save_standard(sender, instance, created, **kwargs):
    if created:
        file_name = instance.web.url.split('/')[-1] 
        unzip_file(file_name)

class AboutUs(models.Model):
    header = models.TextField()
    body = tinymce_models.HTMLField()
    picture = models.ImageField(null=False, blank=False, upload_to="openbanking/aboutus/pictures")
    meta_description = models.CharField(max_length=255)

    
    
    def save(self, *args, **kwargs):
        super(AboutUs, self).save(*args, **kwargs)


    def __str__(self) -> str:
        
        return "About Us" 

class Contributor(models.Model):
    name = models.CharField(max_length=120)
    logo = models.ImageField(null=True, blank=True, upload_to='internation_parners/logos')
    body = tinymce_models.HTMLField()

    def __str__(self) -> str:
        return "{self.name}" 


class Governance(models.Model):
    header = models.TextField()
    body = tinymce_models.HTMLField()
    picture = models.ImageField(null=False, blank=False, upload_to="governance/pictures")
    meta_description = models.CharField(max_length=255)
    
    
    def save(self, *args, **kwargs):
        super(Governance, self).save(*args, **kwargs)


    def __str__(self) -> str:
        return "Governance/მმართველობა" 


class Members(models.Model):
    header = models.TextField()
    body = tinymce_models.HTMLField()
    picture = models.ImageField(null=False, blank=False, upload_to="governance/pictures")
    meta_description = models.CharField(max_length=255)
    members = models.ManyToManyField(Contributor, related_name="open_banking")


    def __str__(self) -> str:
        return "Members"


class Ecosystem(models.Model):
    header = models.TextField()
    body = tinymce_models.HTMLField()
    meta_description = models.CharField(max_length=255)


    def __str__(self) -> str:
        return "Ecosystem"

class Join(models.Model):
    order = models.IntegerField(default=0)
    title = models.CharField(max_length=125)
    body = tinymce_models.HTMLField()

    def __str__(self) -> str:
        return self.title