from django.contrib import admin
from .models import Standard, News, AboutUs, Contributor, Governance, Members, Ecosystem,Join
#froms 
from django import forms


class StandardAdminForm(forms.ModelForm):
    web = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': True}))

    class Meta:
        model = Standard
        fields = "__all__"


class NewsAdminForm(forms.ModelForm):
    body = forms.CharField(widget=forms.Textarea(attrs={'id': "body"}))

    class Meta:
        model = News
        fields = "__all__"

class AboutUsAdminForm(forms.ModelForm):
    body = forms.CharField(widget=forms.Textarea(attrs={'id': "body"}))

    class Meta:
        model = AboutUs
        fields = "__all__"

class ContributorAdminForm(forms.ModelForm):
    body = forms.CharField(widget=forms.Textarea(attrs={'id': "body"}))

    class Meta:
        model = Contributor
        fields = "__all__"

class GovernanceAdminForm(forms.ModelForm):
    body = forms.CharField(widget=forms.Textarea(attrs={'id': "body"}))

    class Meta:
        model = Governance
        fields = "__all__"

class MembersAdminForm(forms.ModelForm):
    body = forms.CharField(widget=forms.Textarea(attrs={'id': "body"}))

    class Meta:
        model = Members
        fields = "__all__"

class EcosystemAdminForm(forms.ModelForm):
    body = forms.CharField(widget=forms.Textarea(attrs={'id': "body"}))

    class Meta:
        model = Ecosystem
        fields = "__all__"

class JoinAdminForm(forms.ModelForm):
    body = forms.CharField(widget=forms.Textarea(attrs={'id': "body"}))

    class Meta:
        model = Join
        fields = "__all__"




# Register your models here.

@admin.register(Standard)
class StandardAdmin(admin.ModelAdmin):
    form = StandardAdminForm


@admin.register(News)
class NewsAdmin(admin.ModelAdmin):
    form = NewsAdminForm


@admin.register(AboutUs)
class AboutUsAdmin(admin.ModelAdmin):
    form = AboutUsAdminForm


@admin.register(Contributor)
class ContributorAdmin(admin.ModelAdmin):
    form = ContributorAdminForm


@admin.register(Governance)
class GovernanceAdmin(admin.ModelAdmin):
    form = GovernanceAdminForm


@admin.register(Members)
class MembersAdmin(admin.ModelAdmin):
    form = MembersAdminForm


@admin.register(Ecosystem)
class EcosystemAdmin(admin.ModelAdmin):
    form = EcosystemAdminForm

@admin.register(Join)
class JoinAdmin(admin.ModelAdmin):
    form = JoinAdminForm