from django.urls import path
from . import views

urlpatterns = [
    path('join/',views.get_join, name='get_join'),
    path('standards/',views.get_standards, name='get_standards'),
]