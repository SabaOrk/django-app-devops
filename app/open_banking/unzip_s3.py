import boto3 
import zipfile 
from datetime import * 
from io import BytesIO 
import json 
import re 

REGION_NAME = 'eu-west-1'
AWS_ACCESS_KEY_ID = 'AKIAQOZXDLUK5NLOV6VT'
AWS_SECRET_ACCESS_KEY = '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'

dev_resource = boto3.resource(
    service_name='s3',
    region_name=REGION_NAME,
    aws_access_key_id=AWS_ACCESS_KEY_ID,
    aws_secret_access_key=AWS_SECRET_ACCESS_KEY
)

session = boto3.Session(
    region_name=REGION_NAME,
    aws_access_key_id=AWS_ACCESS_KEY_ID,
    aws_secret_access_key=AWS_SECRET_ACCESS_KEY
)

dev_client = session.client('s3')

def unzip_file(file_name): 
        
    S3_ZIP_FOLDER = 'openbanking/standards/web/' 

    S3_UNZIPPED_FOLDER = f'openbanking/standards/web_unzipped/{file_name}/' 

    S3_BUCKET = 'img.test.bankingassociation.ge' 

    ZIP_FILE=file_name     

    bucket_dev = dev_resource.Bucket(S3_BUCKET) 

    zip_obj = dev_resource.Object(bucket_name=S3_BUCKET, key=f"{S3_ZIP_FOLDER}{ZIP_FILE}") 

    print("zip_obj=",zip_obj) 

    buffer = BytesIO(zip_obj.get()["Body"].read()) 

    z = zipfile.ZipFile(buffer) 

    #
    # for each file within the zip 

    for filename in z.namelist(): 

        file_info = z.getinfo(filename)   

        # Now copy the files to the 'unzipped' S3 folder 

        print(f"Copying file {filename} to {S3_BUCKET}/{S3_UNZIPPED_FOLDER}{filename}") 

        response = dev_client.put_object( 

            Body=z.open(filename).read() ,

            # might need to replace above line with the one
            # below for windows files 
            # 
            # Body=z.open(filename).read().decode("iso-8859-1").encode(encoding='UTF-8'), 

            Bucket=S3_BUCKET, 

            Key=f'{S3_UNZIPPED_FOLDER}{filename}' 

        ) 
    
    s3_bucket = dev_resource.Bucket(S3_BUCKET)
    for obj in s3_bucket.objects.filter(Prefix='openbanking/standards/web_unzipped/'):
        obj.Acl().put(ACL='public-read')
  

    print(f"Done Unzipping {ZIP_FILE}") 
