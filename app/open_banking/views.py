from rest_framework.decorators import api_view
from rest_framework.response import Response

from . import serializers

from.models import News, Standard, Ecosystem, Join

# Create your views here.

@api_view(['POST'])
def get_article(request):

    url_title = request.data['url_title']


    article = News.objects.get(url_title=url_title)

    data = serializers.serialize_full_article(article)
    articles = [serializers.serialize_article(article) for article in News.objects.all().filter(category=article.category).order_by('-date_created')[:3]]

    return Response({'article': data, 'articles':articles})


@api_view(['POST'])
def get_articles(request):

    curr_month = int(request.data['curr_month']) 
    curr_year = int(request.data['curr_year']) 

    articles = News.objects.all().filter(date_created__month=curr_month,
                                            date_created__year=curr_year).order_by('-date_created')

    data = [serializers.serialize_article(article) for article in articles]

    return Response({'articles': data})


@api_view(['GET'])
def get_standards(request):

    standards = [serializers.serialize_standard(standard) for standard in Standard.objects.all()]

    return Response({ "standards":standards })


@api_view(['GET'])
def get_eco(request):
    eco = serializers.serialize_eco(Ecosystem.objects.all().last())   

    return Response({"data":eco})


@api_view(['GET'])
def get_join(request):
    data = [ serializers.serialize_join(text) for text in Join.objects.all().order_by('order') ]

    return Response({ "data":data })