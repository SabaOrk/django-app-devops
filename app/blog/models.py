from django.db import models
from datetime import date

from tinymce import models as tinymce_models

# Create your models here.
class Category(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField(max_length=160)
    order = models.IntegerField(blank=True,null=True)

    def __str__(self) -> str:
        return self.name


class Article(models.Model):
    date_created = models.DateField(default=date.today)
    category = models.ForeignKey(Category,null=True,blank=True, on_delete=models.SET_NULL, related_name='articles')
    title = models.CharField(max_length=120)
    header = models.TextField()
    body = tinymce_models.HTMLField()
    picture = models.ImageField(null=True, blank=True, upload_to='articles/pictures/')
    url_title = models.CharField(max_length=120)
    meta_title = models.CharField(max_length=255)
    meta_description = models.TextField(default="meta_description")
    

    def __str__(self) -> str:
        return f"{self.title}" 


class Project(models.Model):
    date_created = models.DateField(default=date.today)
    title = models.CharField(max_length=120)
    header = tinymce_models.HTMLField()
    body = tinymce_models.HTMLField()
    picture = models.ImageField(null=True, blank=True, upload_to='projects/pictures/')
    url_title = models.CharField(max_length=120)
    meta_title = models.CharField(max_length=255)
    meta_description = models.TextField(default="meta_description")
    

    def __str__(self) -> str:
        return f"{self.title}" 