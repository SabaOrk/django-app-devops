from django.test import TestCase, Client
from django.urls import reverse
from blog.models import Category, Article, Project
import json


'''
class TestBlogViews(TestCase):

    def setUp(self):
        self.client = Client()
        self.get_article = reverse('get_article')
        self.get_articles = reverse('get_articles')
        self.get_articles_for_home = reverse('get_articles_for_home')

        category = Category.objects.create(
            name='test_category',
            description='description for test category',
            order=1)

        Article.objects.create(category=category,
                               title='Article1 Title',
                               header='Article1 Header',
                               body='Article1 Body',
                               url_title='Article1 URL Title',
                               meta_title='Article1 Meta Title',
                               meta_description='Article1 Meta Description'
                               )

    def test_get_article_views(self):

        response = self.client.post(self.get_article, {'pk': 1})
        self.assertEquals(response.status_code, 200)

    def test_get_articles_views(self):

        response = self.client.post(self.get_articles, {'category': 'test_category',
                                                        'curr_month': 5,
                                                        'curr_year': 2022})
        self.assertEquals(response.status_code, 200)
'''

