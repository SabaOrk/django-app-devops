from django.conf import settings
from .models import Article, Category, Project

url = settings.MEDIA_URL
def serialize_full_article(article):
    
    return {
        'id':article.id,
        'header':article.header,
        'title': article.title,
        'date':article.date_created.strftime('%b. %d, %Y'),
        'category': article.category.name if article.category else '',
        'photo': article.picture.url if article.picture else '',
        'body':article.body,
        'url_title':article.url_title,
        'meta_title':article.meta_title,
        'meta_description':article.meta_description
    }


def serialize_article(article):
    
    return {
        'id':article.id,
        'header':article.header,
        'url_title':article.url_title,
        'title': article.title,
        'date':article.date_created.strftime('%b. %d, %Y'),
        'category': article.category.name if article.category else '',
        'photo': article.picture.url if article.picture else ''
    }

def serialize_project(project):
    
    return{
        'id':project.id,
        'header':project.header,
        'title': project.title,
        'url_title': project.url_title,
        'date':project.date_created.strftime('%b. %d, %Y'),
        'photo': project.picture.url if project.picture else ''
    }

def serialize_full_project(project):
    
    return{
        'id':project.id,
        'header':project.header,
        'body':project.body,
        'title': project.title,
        'date':project.date_created.strftime('%b. %d, %Y'),
        'photo': project.picture.url if project.picture else ''
    }
