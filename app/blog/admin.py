from django.contrib import admin
from .models import Category, Article, Project

#froms 
from django import forms

class ArticleAdminForm(forms.ModelForm):
    body = forms.CharField(widget=forms.Textarea(attrs={'id': "body"}))

    class Meta:
        model = Article
        fields = "__all__"

class ProjectAdminForm(forms.ModelForm):
    header = forms.CharField(widget=forms.Textarea(attrs={'id': "header1"}))
    body = forms.CharField(widget=forms.Textarea(attrs={'id': "body"}))

    class Meta:
        model = Project
        fields = "__all__"

# Register your models here.
@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    pass

@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    list_display=("title","category","date_created")
    search_fields = ("title__icontains", )
    form = ArticleAdminForm

@admin.register(Project)
class ArticleAdmin(admin.ModelAdmin):
    form = ProjectAdminForm