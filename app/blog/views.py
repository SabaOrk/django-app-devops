from rest_framework.decorators import api_view
from rest_framework.response import Response

from . import serializers
from django.conf import settings
from .models import Article, Category, Project



#Views
@api_view(['POST'])
def get_article(request):

    url_title = request.data['url_title']
    print(url_title)

    article = Article.objects.get(url_title=url_title)
    print(article)
    data = serializers.serialize_full_article(article)
    articles = [serializers.serialize_article(article) for article in Article.objects.all().filter(category=article.category).order_by('-date_created')[:3]]

    return Response({'article': data, 'articles':articles})

@api_view(['POST'])
def get_articles(request):
    print(request.data)
    category = request.data['category']
    curr_month = int(request.data['curr_month']) 
    curr_year = int(request.data['curr_year']) 

    category = Category.objects.get(name=category)
    articles = Article.objects.all().filter(category=category,
                                            date_created__month=curr_month,
                                            date_created__year=curr_year).order_by('-date_created')

    data = [serializers.serialize_article(article) for article in articles]

    return Response({'articles': data})

@api_view(['GET'])
def get_articles_for_home(request):

    result = {
        'carousel': [serializers.serialize_article(article) for article in Article.objects.all().order_by('-date_created')[:3]],
        'categories': []
    }

    count = 0

    for cat in Category.objects.all():

        if count == 0:
            result['categories'].append({'name':cat.name,'articles':[serializers.serialize_article(art) for art in cat.articles.all().order_by('date_created')][:5]})

        else:
            result['categories'].append({'name':cat.name,'articles':[serializers.serialize_article(art) for art in cat.articles.all().order_by('date_created')][:2]})

        count += 1
    
    return Response(result)

@api_view(['GET'])
def get_projects(request):

    data = [serializers.serialize_project(project) for project in Project.objects.all().order_by('-date_created')]

    return Response({"projects": data})


@api_view(['POST'])
def get_project(request):
    url_title= request.data['url_title']
    project = serializers.serialize_full_project(Project.objects.get(url_title=url_title)) 
    data = [serializers.serialize_project(proj) for proj in Project.objects.all().order_by('-date_created')[:3] if proj != project]

    return Response({"projects": data, 'project':project})