from django.urls import path
from . import views

urlpatterns = [
    #home
    path('articles_for_home/',views.get_articles_for_home, name='get_articles_for_home'),
    
    #blog
    path('article/', views.get_article, name='get_article'),
    path('articles/', views.get_articles, name='get_articles'),
    path('projects/', views.get_projects, name='get_projects'),
    path('project/', views.get_project, name='get_project'),

]