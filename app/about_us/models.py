from pydoc import describe
from django.db import models
from datetime import date

from tinymce import models as tinymce_models

# Create your models here.

class Person(models.Model):
    full_name = models.CharField(max_length=255,null=False,blank=False)
    job_title = models.CharField(max_length=255,null=False,blank=False)
    description = tinymce_models.HTMLField()
    governance = models.BooleanField(default=False)
    chief = models.BooleanField(default=False)
    picture = models.ImageField(default='persons/pictures/default.png', null=True, blank=True, upload_to='persons/pictures')


class Governance(models.Model):
    header = models.TextField()
    body = tinymce_models.HTMLField()
    picture = models.ImageField(default='governance/pictures/default.png', null=False, blank=False, upload_to='governance/pictures')
    meta_description = models.TextField()

    def __str__(self) -> str:
        return "Governance/მმართველობა" 


class Mission(models.Model):
    header = models.TextField()
    body = tinymce_models.HTMLField()
    picture = models.ImageField(null=False, blank=False, upload_to="governance/pictures")
    meta_description = models.TextField(default="meta_description")
    
    def __str__(self) -> str:
        return "Our Mission/ჩვენი მისია" 


class Committee(models.Model):
    url_title = models.CharField(max_length=120)
    name = models.CharField(max_length=255)
    picture = models.ImageField(default='committees/pictures/default.png' , null=False, blank=False, upload_to="committees/pictures")
    header = models.TextField(default="meta_description")
    body = tinymce_models.HTMLField()
    members = models.ManyToManyField(Person, related_name="committees")
    

class SocialRespArticle(models.Model):
    date_created = models.DateField(default=date.today)
    title = models.CharField(max_length=120)
    header = models.TextField()
    body = tinymce_models.HTMLField()
    picture = models.ImageField(null=True, blank=True, upload_to='articles/pictures')
    url_title = models.CharField(max_length=120)
    meta_title = models.CharField(max_length=255)
    meta_description = models.TextField(default="meta_description")

    def __str__(self) -> str:
        return f"Social Responsibility {self.title}" 


class Partner(models.Model):
    name = models.CharField(max_length=120)
    logo = models.ImageField(default='internation_parners/logos/default.png', null=True, blank=True, upload_to='internation_parners/logos')
    description = models.TextField(default="meta_description")
    international = models.BooleanField(default=False)