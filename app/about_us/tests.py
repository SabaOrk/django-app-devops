from django.test import TestCase, Client
from django.urls import reverse
from about_us.models import Person, Governance, Mission, Committee, SocialRespArticle, Partner
import json


# Create your tests here.

class TestAboutUsViews(TestCase):

    def setUp(self):
        self.client = Client()
        self.get_about_us_article = reverse('get_about_us_article')
        self.get_about_us_articles = reverse('get_about_us_articles')
        self.get_partner_orgs = reverse('get_partner_orgs')
        self.get_mission = reverse('get_mission')
        self.get_committee = reverse('get_committee')
        self.get_committees = reverse('get_committees')
        self.get_governance = reverse('get_governance')

        person1 = Person.objects.create(
                    full_name='Person Personadze',
                    job_title='Person1 Job Title',
                    description='Person1 Description',
                    governance=1,
                    chief=1,
                    )

        mission1 = Mission.objects.create(
                    header='Mission1 Header',
                    body='Mission1 Body',
                    meta_description='Mission1 Meta Description'
                    )
        
        governance1 = Governance.objects.create(
                    header='Mission1 Header',
                    body='Mission1 Body',
                    meta_description='Mission1 Meta Description'
                    )

        partner1 = Partner.objects.create(
                    name='Partner1 Name',
                    international=1,
                    )

        socialresparticle1 = SocialRespArticle.objects.create(
                               title='SocialRespArticle1 Title',
                               header='SocialRespArticle1 Header',
                               body='SocialRespArticle1 Body',
                               url_title='SocialRespArticle1 URL Title',
                               meta_title='SocialRespArticle1 Meta Title',
                               meta_description='SocialRespArticle1 Meta Description',
                               )

        committee1 = Committee.objects.create(
                               name='Comittee1 Name',
                               body='Comittee1 Body',
                               url_title='Comittee URL Title',
                               header='Comittee1 Header',
                               )

        committee1.members.add(person1)

        

    def test_get_about_us_article_views(self):

        response = self.client.post(self.get_about_us_article, {'pk': 1})
        self.assertEquals(response.status_code, 200)

    def test_get_about_us_articles_views(self):

        response = self.client.post(self.get_about_us_articles, {'curr_month': 5, 'curr_year': 2022})
        self.assertEquals(response.status_code, 200)
    
    def test_get_partner_orgs_views(self):

        response = self.client.get(self.get_partner_orgs)
        self.assertEquals(response.status_code, 200)

    def test_get_mission_views(self):

        response = self.client.get(self.get_mission)
        self.assertEquals(response.status_code, 200)
    
    def test_get_committee_views(self):

        response = self.client.post(self.get_committee, {'url_title': 'Comittee URL Title'})
        self.assertEquals(response.status_code, 200)

    def test_get_committees_views(self):

        response = self.client.get(self.get_committees)
        self.assertEquals(response.status_code, 200)

