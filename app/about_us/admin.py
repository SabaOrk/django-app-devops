from django.contrib import admin
from .models import Governance, Mission, Committee, SocialRespArticle, Person, Partner

#froms 
from django import forms

class GovernanceAdminForm(forms.ModelForm):
    body = forms.CharField(widget=forms.Textarea(attrs={'id': "body"}))

    class Meta:
        model = Governance
        fields = "__all__"

class MissionAdminForm(forms.ModelForm):
    body = forms.CharField(widget=forms.Textarea(attrs={'id': "body"}))

    class Meta:
        model = Mission
        fields = "__all__"

class SocialRespArticleAdminForm(forms.ModelForm):
    body = forms.CharField(widget=forms.Textarea(attrs={'id': "body"}))

    class Meta:
        model = SocialRespArticle
        fields = "__all__"


class CommitteeAdminForm(forms.ModelForm):
    body = forms.CharField(widget=forms.Textarea(attrs={'id': "body"}))

    class Meta:
        model = Committee
        fields = "__all__"

class PersonAdminForm(forms.ModelForm):
    description = forms.CharField(widget=forms.Textarea(attrs={'id': "header1"}))

    class Meta:
        model = Person
        fields = "__all__"


# Register your models here.
@admin.register(Governance)
class GovernanceAdmin(admin.ModelAdmin):
    form = GovernanceAdminForm


@admin.register(Mission)
class MissionAdmin(admin.ModelAdmin):
    form = MissionAdminForm


@admin.register(SocialRespArticle)
class SocialRespArticleAdmin(admin.ModelAdmin):
    list_display=("title","header","date_created")
    form = SocialRespArticleAdminForm


@admin.register(Committee)
class CommitteeAdmin(admin.ModelAdmin):
    list_display=("name","header")
    form = CommitteeAdminForm


@admin.register(Person)
class PersonAdmin(admin.ModelAdmin):
    list_display=("full_name","job_title","governance","chief")
    form = PersonAdminForm

@admin.register(Partner)
class PartnerAdmin(admin.ModelAdmin):
    list_display=("name","international")