from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import Governance, Person, Committee, SocialRespArticle, Mission,Partner
from . import serializers
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string


# Create your views here.
@api_view(['GET'])
def get_governance(request):
    
    gov = Governance.objects.all().last()
    chiefs = Person.objects.filter(governance=True,chief=True)
    team = Person.objects.filter(governance=True) 

    team = [serializers.serialize_person(member) for member in team if member not in chiefs]
    chiefs = [serializers.serialize_person(chief) for chief in chiefs]

    return Response({'governance':serializers.serialize_gov(gov), "chiefs":chiefs,'team':team})


@api_view(['GET']) 
def get_committees(request):
    
    comms = [serializers.serialize_comm(committee) for committee in Committee.objects.all()]

    return Response({'committees': comms})


@api_view(['POST'])
def get_committee(request):

    url_title = request.data['url_title']
    committee = Committee.objects.filter(url_title=url_title)[0]
    committees = Committee.objects.all()
    committees = [serializers.serialize_comm(comm) for comm in committees if comm != committee and len(committees) <= 4 ]
    committee = serializers.serialize_committee(committee)

    return Response({
        'committees' : committees,
        'committee' : committee
    })


@api_view(['GET'])
def get_mission(request):

    our_mission = Mission.objects.all().last()
    return Response({'governance': serializers.serialize_gov(our_mission)})


@api_view(['GET'])
def get_partner_orgs(request):
    int_partners = [serializers.serialize_partners(partner) for partner in Partner.objects.filter(international=True)]
    loc_partners = [serializers.serialize_partners(partner) for partner in Partner.objects.filter(international=False) ]
    return Response({
        'int_partners':int_partners,
        'loc_partners':loc_partners
        })


@api_view(['POST'])
def get_about_us_article(request):

    pk = request.data['pk']
    article = SocialRespArticle.objects.get(pk=pk)
    data = serializers.serialize_article(article)

    return Response({'article': data})


@api_view(['POST'])
def get_about_us_articles(request):

    curr_month = int(request.data['curr_month']) 
    curr_year = int(request.data['curr_year']) 

    articles = SocialRespArticle.objects.all().filter(
        date_created__month=curr_month,
        date_created__year=curr_year
        ).order_by('-date_created')

    data = [serializers.serialize_article(article) for article in articles]

    return Response({'articles': data})


@api_view(['POST'])
def send_contact_email(request):

    first_name = request.data['first_name']
    last_name = request.data['last_name']
    message = request.data['message']
    recipient = request.data['send_to']

	#email to user
    mail_subject = 'Thanks for reaching out to us!'
    mail_body = ''
    msg = EmailMultiAlternatives(mail_subject, mail_body, settings.EMAIL_HOST_USER, [send_to])
    msg.send()

    chveni_email = 'orkoshnelisaba@gmail.com'
    subject = 'New Contact email!'
    email = EmailMultiAlternatives(subject, message, settings.EMAIL_HOST_USER, ['orkoshnelisaba@gmail.com'])
    email.send()
	
    response = {"success": 'true', "message": f'email sent to {send_to}, {chveni_email}', "data": {}}

    return Response(response)