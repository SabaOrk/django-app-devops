from django.conf import settings
from about_us.models import Person, Governance, Mission, Committee, SocialRespArticle, Partner

url = settings.MEDIA_URL

def serialize_gov(obj):
    return{
        'header':obj.header,
        'body':obj.body,
        'picture': obj.picture.url if obj.picture else '',
    }

def serialize_person(obj):
    return{
        'full_name':obj.full_name,
        'job_title':obj.job_title,
        'description':obj.description,
        'picture': obj.picture.url if obj.picture else '',
    }

def serialize_member(obj):
    return{
        'full_name':obj.full_name,
        'job_title':obj.job_title
    }

def serialize_partners(obj):
    return{
        'name':obj.name,
        'logo': obj.logo.url if obj.logo else '',
        'description':obj.description
    }

def serialize_comm(obj):
    return{
        'url_title':obj.url_title,
        'name': obj.name,
        'header':obj.header,
        'picture': obj.picture.url if obj.picture else '',
    }

def serialize_committee(obj):
    members = Person.objects.all().filter(committees=obj)
    return{
        'title': obj.name,
        'header' : obj.header,
        'body' : obj.body,
        'photo': obj.picture.url if obj.picture else '',
        'members': [serialize_member(member) for member in members]   
    }

def serialize_article(article):
    
    return {
        'id':article.id,
        'header':article.header,
        'title': article.title,
        'date':article.date_created.strftime('%b. %d, %Y'),
        'photo': article.picture.url if article.picture else ''
    }

def serialize_full_article(article):
    
    return {
        'id':article.id,
        'header':article.header,
        'title': article.title,
        'date':article.date_created.strftime('%b. %d, %Y'),
        'photo': article.picture.url if article.picture else '',
        'body':article.body,
        'url_title':article.url_title,
        'meta_title':article.meta_title,
        'meta_description':article.meta_description
    }

