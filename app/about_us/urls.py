from django.urls import path
from . import views

urlpatterns = [
    
    path('governance/', views.get_governance, name='get_governance'),
    path('committees/', views.get_committees, name='get_committees'),
    path('committee/', views.get_committee, name='get_committee'),
    path('mission/', views.get_mission, name='get_mission'),
    path('partners/', views.get_partner_orgs, name='get_partner_orgs'),
    path('article/', views.get_about_us_article, name='get_about_us_article'),
    path('articles/', views.get_about_us_articles, name='get_about_us_articles'),
    path('sendemail/', views.send_contact_email, name='send_contact_email'),
]