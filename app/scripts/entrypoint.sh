#!/bin/sh

set -e
python manage.py makemigrations
python manage.py migrate --noinput
#python manage.py collectstatic --noinput
#python manage.py test --noinput 
gunicorn app.wsgi:application --bind 0.0.0.0:8000

#uwsgi --socket :8000 --workers 4 --master --enable-threads --module app.wsgi

