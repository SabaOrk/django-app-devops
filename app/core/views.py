from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.db import connections
from django.conf import settings


@api_view(['GET'])
def health_check(request):
    conn = connections['default']
    try:
        c = conn.cursor()
    except OperationalError:
        return Response({'db_connection':'failed'}, 400)
    else:
        return Response({'db_connection':'success'}, 200)