from django.urls import path
from . import views
from django.views.generic.base import TemplateView
from django.conf import settings

urlpatterns = [
    path('health/', views.health_check, name='health_check'),
    path('test/', TemplateView.as_view(template_name='index.html'), name='test'),
]